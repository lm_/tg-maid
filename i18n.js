﻿const Strings = {
  'en': require('./lang/en.json'),
  'ru': require('./lang/ru.json'),
  //'jp': require('./lang/jp.json'),
}

// https://stackoverflow.com/posts/69718380
String.prototype.format = function (args) {
  return this.replace(/{([0-9]+)}/g, function (match, index) {
    // check if the argument is there
    return typeof args[index] == 'undefined' ? match : args[index];
  });
};

function get() {
  const lang = arguments[0] || 'en';
  const key = arguments[1];
  let args = Object.values(arguments);
  args.splice(0, 2);
  if (Strings[lang][key] == null) {
    return 'i18n error: string "{0}" not found'.format([ key ]);
  }
  if (typeof(Strings[lang][key]) == 'object') {
    return Strings[lang][key];
  }
  return Strings[lang][key].format(args);
}

module.exports = { get: get };
