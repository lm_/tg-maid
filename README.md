Telegram bot that acts as a personal assistant for task and time management. Built using Telegraf framework: [https://github.com/telegraf/telegraf/](https://github.com/telegraf/telegraf/).  

## Installation
 * install Node.js
 * pull this repository
 * `npm install telegraf`
 * `npm install telegraf-session-local`
 * `npm install telegraf-calendar-telegram`
 * `npm install full-icu`
 
## Configuration
`config.json`
```
{
  "token": "0000:XXx-xXXxxXx-xXxXxX"
}
```
**token** - access token to hook up a particular bot

## Usage
Create a Telegram bot and configure it to disable groups (make it personal only), then copy the API token and paste it to the `config.json` (if there's no such file, create one using the template above or generate one by running the bot once) and run the bot using either `node` or `npm` CLI.  
With node:  
`node --icu-data-dir=node_modules/full-icu main.js`  
  
With npm:  
`npm run bot`
  
## Running as a service
### Running with systemd:
1. Create file `/etc/systemd/system/tg-maid.service` with the following contents:
```
[Unit]
Description=tg-maid

[Service]
ExecStart=/usr/local/node/bin/node --icu-data-dir=/usr/src/tg-maid/node_modules/full-icu /usr/src/tg-maid/main.js
Restart=on-failure
WorkingDirectory=/usr/src/tg-maid

[Install]
WantedBy=multi-user.target
```
2. `chmod +x /usr/src/tg-maid/main.js`
3.
```
sudo systemctl daemon-reload
sudo systemctl enable tg-maid.service
sudo systemctl start tg-maid.service
```

### (OLD) Running with [forever-service](https://github.com/zapty/forever-service): 
```
npm install -g forever
npm install -g forever-service
cd (tg-maid directory)
(sudo) forever-service install tg-maid -s main.js -f " -c '/usr/local/bin/node --icu-data-dir=path/to/tg-maid/node_modules/full-icu'"
(sudo) service tg-maid start
```
