﻿const fs = require("fs");
require("full-icu");
const Telegraf = require('telegraf');
const LocalSession = require('telegraf-session-local');
const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');
const Composer = require('telegraf/composer');
const Stage = require('telegraf/stage');
const Scene = require('telegraf/scenes/base');
const WizardScene = require('telegraf/scenes/wizard');
const Calendar = require('telegraf-calendar-telegram');
const TimeZones = require("./timezones.json");  // source: https://stackoverflow.com/a/54500197
const { enter, leave } = Stage;
const i18n = require('./i18n.js');

let _bot;
let _session;
let _calendar;

const _default_tz = "Europe/London";
const _hour_ms = 3600000;

const Emotion = {};
Emotion.GREET = "https://i.imgur.com/BWxARi8.jpg";
Emotion.ATTENTION = "https://i.imgur.com/HvhYDQo.jpg";
Emotion.GLAD = "https://i.imgur.com/vn2yeym.jpg";
Emotion.SURPRISE = "https://i.imgur.com/3UMMFqv.jpg";
Emotion.DISAPPOINT = "https://i.imgur.com/WWGoPSn.jpg";
Emotion.UPSET = "https://i.imgur.com/mN8Ykwl.jpg";
Emotion.CRINGE = "https://i.imgur.com/cNgIqwG.jpg";
Emotion.INFODUMP = "https://i.imgur.com/Qj9eBiR.jpg";
Emotion.EXCUSE = "https://i.imgur.com/qlfnm0q.jpg";
Emotion.EXPLAIN = "https://i.imgur.com/2gbZYyi.jpg";
Emotion.CONCENTRATE = "https://i.imgur.com/QtOPR5T.jpg";
Emotion.SPECIFY = "https://i.imgur.com/ziKjOyl.jpg";
Emotion.PISSED = "https://i.imgur.com/WO0wgTt.jpg";
Emotion.REWARD = "https://i.imgur.com/1fijlCm.jpg";
Emotion.ANGRY = "https://i.imgur.com/JxA4eFQ.jpg";

const Interval = {};
Interval.ONCE = 0;
Interval.DAILY = 1;
Interval.WEEKLY = 2;
Interval.WEEKDAYS = 3;
Interval.WEEKEND = 4;

const GenderIcons = {
  master: '👨',
  mistress: '👩'
}

function checkString(str) {
  if (str.length > 60) {
    return false;
  }
  var safe = false;
  try {
    JSON.parse('["' + str + '"]');
    safe = true;
  } catch (err) {};
  return safe;
}

function checkNumber(str) {
  if (isNaN(str)) {
    return false;
  }
  if (+str > 168) {
    return false;
  }
  return +str;
}

function getTimezoneOffset(d, tz) {
  const a = d.toLocaleString("ja", {timeZone: tz}).split(/[/\s:]/);
  a[1]--;
  const t1 = Date.UTC.apply(null, a);
  const t2 = new Date(d).setMilliseconds(0);
  return t2 - t1;
}

function getDailyResetTime(offset) {
  let o = 24 + offset/1000/60/60;
  if (o > 24) {
    o = o - 24;
  }
  return o;
}

function dailyResetTime(now, offset) {
  let itsTime = false;
  if (now.getHours() ==  getDailyResetTime(offset)) {
    if (now.getMinutes() == 0) {
      itsTime = true;
    }
  }
  return itsTime;
}

function getMainMenu(ctx) {
  return  Markup.inlineKeyboard([
    [ Markup.callbackButton(i18n.get(ctx.session.lang, 'tasks_show'), 'task_list') ],
    [ Markup.callbackButton(i18n.get(ctx.session.lang, 'tasks_edit'), 'task_edit_list') ],
    [ Markup.callbackButton(i18n.get(ctx.session.lang, 'set_timezone') + ' ' + ctx.session.timezone + ' ' + i18n.get(ctx.session.lang, 'change'), 'set_timezone') ],
    [ Markup.callbackButton(i18n.get(ctx.session.lang, 'help'), 'help') ],
    [ Markup.callbackButton(GenderIcons[ctx.session.gender] + ' ' + i18n.get(ctx.session.lang, 'gender'), 'gender') ],
    [ Markup.callbackButton(i18n.get(ctx.session.lang, 'flag') + ' ' + i18n.get(ctx.session.lang, 'lang'), 'lang') ]
  ]);
}

function notImplemented(ctx) {
  ctx.replyWithPhoto(Emotion.EXCUSE, { caption: i18n.get(ctx.session.lang, 'not_implemented', i18n.get(ctx.session.lang, ctx.session.gender)) }).then(function() {
    let scene = ctx.session.__scenes.current;
    if (scene == "mainMenuWizard") {
      ctx.scene.leave().then(function() {
        ctx.scene.enter('mainMenuRepeatWizard');
      });
    } else if (scene == "mainMenuRepeatWizard"||scene == "taskEditListRepeatWizard") {
      ctx.scene.reenter();
    } else if (scene == "taskEditListWizard") {
      ctx.scene.leave().then(function() {
        ctx.scene.enter('taskEditListRepeatWizard');
      });
    }
  });
}

function back(ctx) {
  let scene = ctx.session.__scenes.current;
  if (scene == "taskEditListWizard"||scene == "taskEditListRepeatWizard"||scene=="taskListWizard"||scene=="taskListRepeatWizard") {
    ctx.scene.leave().then(function() {
      ctx.scene.enter('mainMenuRepeatWizard');
    });
  }
}

function insertTask(ctx) {
  let id = 0;
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id }).value();
  for(let i = 0; i < tasks.length; i++) {
    if (tasks[i].id >= id) {
      id = tasks[i].id + 1;
    }
  }
  let task = ctx.wizard.state.add_task;
  task.id = id;
  task.user = ctx.chat.id;
  task.progress = 0;
  task.track = false;
  task.notify = false;
  task.remind = false;
  task.remove = false;
  let today = new Date();
  today.setHours(0,0,0,0);
  task.reset = (today.getTime() + _hour_ms*24)/1000;
  task.enabled = true;
  ctx['sessionDB'].get('tasks').push(task).write();
}

function updateTask(user, task) {
  _session.DB.get("tasks").updateWhere({ user: user, id: task.id }, { 
    track: task.track,
    reset: task.reset,
    remind: task.remind,
    notify: task.notify,
    progress: task.progress
  }).write();
}

function sendReminder(mode, session, offset, tasks) {
  let reply = "";
  let image;
  if (tasks[0].interval == Interval.ONCE) {
    if (mode == "remind") {
      reply = reply + i18n.get(session.data.lang, 'remind_once', i18n.get(session.data.lang, session.data.gender));
    } else {
      reply = reply + i18n.get(session.data.lang, 'notify_once', i18n.get(session.data.lang, session.data.gender));
    }
    image = Emotion.SPECIFY;
  } else {
    if (mode == "remind") {
      reply = reply + i18n.get(session.data.lang, 'remind_daily', i18n.get(session.data.lang, session.data.gender));
      image = Emotion.EXPLAIN;
    } else if (mode == "scold") {
      reply = reply + i18n.get(session.data.lang, 'scold_daily', i18n.get(session.data.lang, session.data.gender));
      image = Emotion.DISAPPOINT;
    } else if (mode == "notify") {
      reply = reply + i18n.get(session.data.lang, 'notify_daily', i18n.get(session.data.lang, session.data.gender));
      image = Emotion.GLAD;
    }
  }
  reply = reply + '\n';
  for (let i = 0; i < tasks.length; i++) {
    reply = reply + (i + 1) + '. ';
    reply = reply + tasks[i].name + " | ";
    reply = reply + i18n.get(session.data.lang, 'interval_' + tasks[i].interval) +  ' | ';
    if (tasks[i].interval == Interval.ONCE) {
      let date = new Date(tasks[i].timestamp);
      reply = reply + date.toLocaleDateString(i18n.get(session.data.lang, 'locale')) + ' ' + new Date(date.getTime() - offset).toLocaleTimeString(i18n.get(session.data.lang, 'locale'), { hour: '2-digit', minute: '2-digit' });
    } else {
      reply = reply + tasks[i].limit +  ' ' + i18n.get(session.data.lang, 'hours');
    }
    if (i < tasks.length - 1) {
      reply = reply + '\n';
    }
  }
  _bot.telegram.sendPhoto(parseInt(session.id.split(':')[1]), image, { caption: reply });
}

function sendReward(session) {
  let image;
  let reply;
  if (session.data.gender == 'master') {
    image = Emotion.REWARD;
    reply = i18n.get(session.data.lang, 'reward_master', i18n.get(session.data.lang, 'master'));
  } else if (session.data.gender == 'mistress') {
    image = Emotion.GLAD;
    reply = i18n.get(session.data.lang, 'reward_mistress', i18n.get(session.data.lang, 'mistress'));
  }
  _bot.telegram.sendPhoto(parseInt(session.id.split(':')[1]), image, { caption: reply });
}

function checkLoop() {
  const sessions = _session.DB.get('sessions').value();
  const all_tasks = _session.DB.get('tasks');
  let now = new Date();
  for (let i = 0; i < sessions.length; i++) {
    let notify = [];
    let remind = [];
    let scold = [];
    let current = 0;
    let completed = 0;
    let user = parseInt(sessions[i].id.split(':')[1]);
    let today = new Date();
    today.setHours(0,0,0,0);
    let offset = getTimezoneOffset(now, sessions[i].data.timezone);
    let weekend = now.getDay() == 6 || now.getDay() == 0;
    let task_date;
    let reset;
    let tasks = all_tasks.filter({ user: user, enabled: true }).value();
    for (j = 0; j < tasks.length; j++) {
      if (tasks[j].interval == Interval.ONCE) {
        task_date = new Date(tasks[j].timestamp);
        task_date.setHours(0,0,0,0);
        if (task_date.getTime() == today.getTime()) {
          task_date = new Date(tasks[j].timestamp);
          if (now >= task_date) {
            if (!tasks[j].notify) {
              tasks[j].notify = true;
              tasks[j].remind = true;
              updateTask(user, tasks[j]);
              notify.push(tasks[j]);
            }
          } else {
            if ((task_date.getTime() - now.getTime()) <= _hour_ms) {
              if (!tasks[j].remind) {
                tasks[j].remind = true;
                updateTask(user, tasks[j]);
                remind.push(tasks[j]);
              }
            }
          }
        }
      } else if (tasks[j].interval == Interval.DAILY) {
        current = current + 1;
        if (dailyResetTime(now, offset)) {
          if (tasks[j].progress < tasks[j].limit) {
            scold.push(tasks[j]);
          }
        //  tasks[j].reset = today.getTime()/1000;
          tasks[j].remind = false;
          tasks[j].notify = false;
          tasks[j].progress = 0;
          tasks[j].track = false;
          updateTask(user, tasks[j]);
          remind.push(tasks[j]);
        } else {
          if (tasks[j].track) {
            let track_progress = +((new Date().getTime() - new Date(tasks[j].track_since).getTime())/1000/60/60).toFixed(2);
            tasks[j].progress = tasks[j].progress + track_progress;
            tasks[j].track_since = Math.round(new Date().getTime());
          }
          if (tasks[j].progress >= tasks[j].limit) {
            completed = completed + 1;
            if (!tasks[j].notify) {
              tasks[j].notify = true;
              updateTask(user, tasks[j]);
              notify.push(tasks[j]);
            }
          }
        }
      } else if (tasks[j].interval == Interval.WEEKLY) {
        if (now.getDay() == 1) {
          if (dailyResetTime(now, offset)) {
            if (tasks[j].progress < tasks[j].limit) {
              scold.push(tasks[j]);
            }
          //  tasks[j].reset = today.getTime()/1000;
            tasks[j].remind = false;
            tasks[j].notify = false;
            tasks[j].progress = 0;
            tasks[j].track = false;
            updateTask(user, tasks[j]);
            remind.push(tasks[j]);
          } else {
            if (tasks[j].track) {
              let track_progress = +((new Date().getTime() - new Date(tasks[j].track_since).getTime())/1000/60/60).toFixed(2);
              tasks[j].progress = tasks[j].progress + track_progress;
              tasks[j].track_since = Math.round(new Date().getTime());
            }
            if (tasks[j].progress >= tasks[j].limit) {
              if (!tasks[j].notify) {
                tasks[j].notify = true;
                updateTask(user, tasks[j]);
                notify.push(tasks[j]);
              }
            }
          }
        }
      } else if (tasks[j].interval == Interval.WEEKDAYS) {
        if (!weekend) {
          current = current + 1;
          if (dailyResetTime(now, offset)) {
            if (tasks[j].progress < tasks[j].limit) {
              scold.push(tasks[j]);
            }
          //  tasks[j].reset = today.getTime()/1000;
            tasks[j].remind = false;
            tasks[j].notify = false;
            tasks[j].progress = 0;
            tasks[j].track = false;
            updateTask(user, tasks[j]);
            remind.push(tasks[j]);
          } else {
            if (tasks[j].track) {
              let track_progress = +((new Date().getTime() - new Date(tasks[j].track_since).getTime())/1000/60/60).toFixed(2);
              tasks[j].progress = tasks[j].progress + track_progress;
              tasks[j].track_since = Math.round(new Date().getTime());
            }
            if (tasks[j].progress >= tasks[j].limit) {
              completed = completed + 1;
              if (!tasks[j].notify) {
                tasks[j].notify = true;
                updateTask(user, tasks[j]);
                notify.push(tasks[j]);
              }
            }
          }
        }
      } else if (tasks[j].interval == Interval.WEEKEND) {
        if (weekend) {
          current = current + 1;
          if (dailyResetTime(now, offset)) {
            if (tasks[j].progress < tasks[j].limit) {
              scold.push(tasks[j]);
            }
          //  tasks[j].reset = today.getTime()/1000;
            tasks[j].remind = false;
            tasks[j].notify = false;
            tasks[j].progress = 0;
            tasks[j].track = false;
            updateTask(user, tasks[j]);
            remind.push(tasks[j]);
          } else {
            if (tasks[j].track) {
              let track_progress = +((new Date().getTime() - new Date(tasks[j].track_since).getTime())/1000/60/60).toFixed(2);
              tasks[j].progress = tasks[j].progress + track_progress;
              tasks[j].track_since = Math.round(new Date().getTime());
            }
            if (tasks[j].progress >= tasks[j].limit) {
              completed = completed + 1;
              if (!tasks[j].notify) {
                tasks[j].notify = true;
                updateTask(user, tasks[j]);
                notify.push(tasks[j]);
              }
            }
          }
        }
      }
    }
    if (scold.length > 0) {
      sendReminder("scold", sessions[i], offset, scold);
    }
    if (notify.length > 0) {
      sendReminder("notify", sessions[i], offset, notify);
    }
    if (remind.length > 0) {
      sendReminder("remind", sessions[i], offset, remind);
    }
    if (current > 0) {
      if (current == completed) {
        if (sessions[i].data.reward != today.getTime()/1000) {
          sessions[i].data.reward = today.getTime()/1000;
          sendReward(sessions[i]);
        }
      }
    }
  }
  _session.DB.get("tasks").removeWhere({ remove: true }).write();
}

function dateListener(ctx, date) {
  ctx.session.date = date;
  ctx.scene.enter(ctx.session.next_scene);
}

function showCalendar(ctx) {
  _calendar = new Calendar(_bot, { startWeekDay: 1, weekDayNames: i18n.get(ctx.session.lang, 'weekday_names'), monthNames: i18n.get(ctx.session.lang, 'month_names') });
  _calendar.setDateListener(dateListener);
  const today = new Date();
	const min_date = new Date();
	min_date.setMonth(today.getMonth() - 2);
	const max_date = new Date();
	max_date.setMonth(today.getMonth() + 2);
	max_date.setDate(today.getDate());
  ctx.reply(i18n.get(ctx.session.lang, 'add_task_date', i18n.get(ctx.session.lang, ctx.session.gender)), _calendar.setMinDate(min_date).setMaxDate(max_date).getCalendar());
}

function getTaskEditList(ctx) {
  let reply;
  let image;
  
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id, remove: false }).value();
  if (tasks.length > 0) {
    image = Emotion.INFODUMP;
    reply = i18n.get(ctx.session.lang, 'edit_list', i18n.get(ctx.session.lang, ctx.session.gender)) + "\n";
    for (let i = 0; i < tasks.length; i++) {
      if (!tasks[i].enabled) {
        reply = reply + '<strike>';
      }
      reply = reply + (i + 1) + ". " + tasks[i].name +  " | ";
      reply = reply + i18n.get(ctx.session.lang, 'interval_' + tasks[i].interval) +  " | ";
      if (tasks[i].interval == Interval.ONCE) {
        let date = new Date(tasks[i].timestamp);
        let offset = getTimezoneOffset(date, ctx.session.timezone);
        reply = reply + date.toLocaleDateString(i18n.get(ctx.session.lang, 'locale')) + " " + new Date(date.getTime() - offset).toLocaleTimeString(i18n.get(ctx.session.lang, 'locale'), { hour: '2-digit', minute: '2-digit' });
      } else {
        reply = reply + tasks[i].limit +  " " + i18n.get(ctx.session.lang, 'hours');
      }
      if (!tasks[i].enabled) {
        reply = reply + '</strike>';
      }
      if (i < tasks.length - 1) {
        reply = reply + '\n';
      }
    }
  } else {
    image = Emotion.EXCUSE;
    reply = i18n.get(ctx.session.lang, 'no_tasks', i18n.get(ctx.session.lang, ctx.session.gender));
  }
  let markup = [];
  if (tasks.length > 0) {
    markup.push([ Markup.callbackButton(i18n.get(ctx.session.lang, 'task_add'), 'task_add'), Markup.callbackButton(i18n.get(ctx.session.lang, 'task_edit'), 'task_edit') ]);
    markup.push([ Markup.callbackButton(i18n.get(ctx.session.lang, 'task_disable'), 'task_disable'), Markup.callbackButton(i18n.get(ctx.session.lang, 'task_enable'), 'task_enable') ]);
    markup.push([ Markup.callbackButton(i18n.get(ctx.session.lang, 'task_remove'), 'task_remove'), Markup.callbackButton(i18n.get(ctx.session.lang, 'back'), 'back') ]);
  } else {
    markup.push([ Markup.callbackButton(i18n.get(ctx.session.lang, 'task_add'), 'task_add'), Markup.callbackButton(i18n.get(ctx.session.lang, 'back'), 'back') ]);
  }  
  return [ image, reply, markup ];
}

function getCurrentTaskList(ctx) {
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id, enabled: true, remove: false }).value();
  let date = new Date();
  let offset = getTimezoneOffset(date, ctx.session.timezone);
  date = new Date(date.getTime() - offset);
  date.setHours(0,0,0,0);
  let time = new Date();
  let t = [];
  let task_date;
  let task_time;
  let weekend = false;
  if (date.getDay() == 6 || date.getDay() == 0) {
    weekend = true;
  }
  for (let i = 0; i < tasks.length; i++) {
    task_date = new Date(tasks[i].timestamp);
    task_date = new Date(task_date.getTime() - offset);
    task_date.setHours(0,0,0,0);
    if (tasks[i].interval == Interval.ONCE) {
      if (task_date.getTime() == date.getTime()) {
        task_time = new Date(tasks[i].timestamp);
        if (time >= task_time) {
          tasks[i].progress = 1;
        }
        t.push(tasks[i]);
      }
    } else if (tasks[i].interval == Interval.DAILY) {
      t.push(tasks[i]);
    } else if (tasks[i].interval == Interval.WEEKDAYS) {
      if (!weekend) {
        t.push(tasks[i]);
      }
    } else if (tasks[i].interval = Interval.WEEKEND) {
      if (weekend) {
        t.push(tasks[i]);
      }
    }
  }
  return t;
}

function getTaskList(ctx) {
  let tasks = getCurrentTaskList(ctx);
  let image;
  let reply;
  let is_tracking = false;
  if (tasks.length > 0) {
    image = Emotion.INFODUMP;
    reply = i18n.get(ctx.session.lang, 'list', i18n.get(ctx.session.lang, ctx.session.gender)) + '\n';
    for (let i = 0; i < tasks.length; i++) {
      let progress = tasks[i].progress;
      if (tasks[i].track) {
        reply = reply + '⏱ ';
        is_tracking = true;
        const track_progress = +((new Date().getTime() - new Date(tasks[i].track_since).getTime())/1000/60/60).toFixed(2);
        progress = progress + track_progress;
      }
      if (progress >= tasks[i].limit) {
        reply = reply + '<strike>';
      }
      reply = reply + (i + 1) + '. ' + tasks[i].name +  ' | ';
      reply = reply + i18n.get(ctx.session.lang, 'interval_' + tasks[i].interval) +  ' | ';
      if (tasks[i].interval == Interval.ONCE) {
        let date = new Date(tasks[i].timestamp);
        let offset = getTimezoneOffset(date, ctx.session.timezone);
        reply = reply + date.toLocaleDateString(i18n.get(ctx.session.lang, 'locale')) + ' ' + new Date(date.getTime() - offset).toLocaleTimeString(i18n.get(ctx.session.lang, 'locale'), { hour: '2-digit', minute: '2-digit' });
      } else {
        reply = reply + progress.toFixed(2) +  '/' + tasks[i].limit + ' ' + i18n.get(ctx.session.lang, 'hours');
      }
      if (progress >= tasks[i].limit) {
        reply = reply + '</strike>';
        reply = reply + ' ✅';
      }
      if (i < tasks.length - 1) {
        reply = reply + '\n';
      }
    }
  } else {
    image = Emotion.EXCUSE;
    reply = i18n.get(ctx.session.lang, 'no_pending_tasks', i18n.get(ctx.session.lang, ctx.session.gender));
  }
  let markup = [];
  if (tasks.length > 0) {
    for (let i = 0; i < tasks.length; i++) {
      if (tasks[i].interval!=Interval.ONCE) {
        markup.push([ Markup.callbackButton(i18n.get(ctx.session.lang, 'track_start'), 'track_start') ]);
        break;
      }
    }
  }
  if (is_tracking) {
    markup.push([ Markup.callbackButton(i18n.get(ctx.session.lang, 'track_stop'), 'track_stop') ]);
  }
  markup.push([ Markup.callbackButton(i18n.get(ctx.session.lang, 'back'), 'back') ]);
  return [ image, reply, markup ];
}

const taskSelectHandler = new Composer();
taskSelectHandler.action(/^[task]+(-[0-9]+)?$/, function(ctx) {
  ctx.wizard.state.select_id = +ctx.match[1].split('-')[1];
  ctx.wizard.next();
  return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});


const taskTrackWizard = new WizardScene('taskTrackWizard', function(ctx){
  const tasks = getCurrentTaskList(ctx);
  let t = [];
  for (let i = 0; i < tasks.length; i++) {
    if (!tasks[i].track) {
      if (tasks[i].interval != Interval.ONCE) {
        t.push(tasks[i]);
      }
    }
  }
  let markup = [];
  for (let i = 0; i < t.length; i++) {
    markup.push([ Markup.callbackButton(t[i].name, "task-" + t[i].id) ]);
  }
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'track_choice'), { reply_markup: Markup.inlineKeyboard(markup) });
  return ctx.wizard.next();
}, taskSelectHandler, function(ctx) {
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id }).value();
  for (let i = 0; i < tasks.length; i++) {
    if (tasks[i].track) {
      ctx['sessionDB'].get('tasks').updateWhere({ user: ctx.chat.id, id: tasks[i].id }, { track: false }).write();
    }
  }
  ctx['sessionDB'].get('tasks').updateWhere({ user: ctx.chat.id, id: ctx.wizard.state.select_id }, { track: true, track_since: Math.round(new Date().getTime()) }).write();
  ctx.scene.leave().then(function() {
    ctx.scene.enter("taskListRepeatWizard");
  });
});

const taskListHandler = new Composer();
taskListHandler.action('track_start', function(ctx) {
  ctx.scene.enter('taskTrackWizard');
});
taskListHandler.action('track_stop', function(ctx) {
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id }).value();
  for (let i = 0; i < tasks.length; i++) {
    if (tasks[i].track) {
      ctx['sessionDB'].get('tasks').updateWhere({ user: ctx.chat.id, id: tasks[i].id }, { track: false }).write();
    }
  }
  ctx.scene.leave().then(function() {
    ctx.scene.enter("taskListRepeatWizard");
  });
});
taskListHandler.action('back', back);

const taskListWizard = new WizardScene('taskListWizard', function(ctx){
  const image_reply_markup = getTaskList(ctx);
  ctx.replyWithPhoto(image_reply_markup[0], { caption: image_reply_markup[1],  parse_mode: 'HTML', reply_markup: Markup.inlineKeyboard(image_reply_markup[2]) });
  return ctx.wizard.next()
}, taskListHandler);

const taskListRepeatWizard = new WizardScene('taskListRepeatWizard', function(ctx){
  const image_reply_markup = getTaskList(ctx);
  ctx.reply(image_reply_markup[1], { parse_mode: 'HTML' }).then(function() {
    ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'repeat'), { reply_markup: Markup.inlineKeyboard(image_reply_markup[2]) });
    ctx.wizard.next();
  });
}, taskListHandler);


const addOneTimeTaskWizard = new WizardScene('addOneTimeTaskWizard', function(ctx){
  delete ctx.session.next_scene;
  ctx.reply(i18n.get(ctx.session.lang, 'add_task_time', i18n.get(ctx.session.lang, ctx.session.gender)));
  return ctx.wizard.next();
}, function(ctx) {
  let time = ctx.message.text;
  if(!/^(2[0-3]|[0-1]?[\d]):[0-5][\d]$/.test(time)) {
    ctx.replyWithPhoto(Emotion.SPECIFY, { caption: i18n.get(ctx.session.lang, 'invalid_limit') });
    return;
  }
  let date = new Date(Date.parse(ctx.session.date));
  date.setHours(time.split(':')[0], time.split(':')[1]);
  let offset = getTimezoneOffset(date, ctx.session.timezone);
  ctx.wizard.state.add_task = ctx.session.add_task;
  ctx.wizard.state.add_task.timestamp = Math.floor(date.getTime() + offset);
  ctx.wizard.state.add_task.interval = Interval.ONCE;
  ctx.wizard.state.add_task.limit = 1;
  delete ctx.session.add_task;
  delete ctx.session.date;
  insertTask(ctx);
  let reply = ctx.wizard.state.add_task.name + i18n.get(ctx.session.lang, 'comma');
  reply = reply + " " + date.toLocaleDateString(i18n.get(ctx.session.lang, 'locale'));
  reply = reply + i18n.get(ctx.session.lang, 'done');
  ctx.replyWithPhoto(Emotion.GLAD, { caption: reply }).then(function(){
    ctx.scene.leave().then(function() {
      ctx.scene.enter("taskEditListRepeatWizard");
    });
  });
});

const nameConfirmHandler = new Composer();

nameConfirmHandler.action('yes', function(ctx) {
  ctx.wizard.next();
  return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});

nameConfirmHandler.action('no', function(ctx) {
  ctx.wizard.back();
  ctx.wizard.back();
  return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});

const taskIntervalHandler = new Composer();
taskIntervalHandler.action(/^[interval]+(-[a-z]+)?$/, function(ctx) {
  let i = ctx.match[1].split('-')[1].toUpperCase();
  let next_scene = "addOneTimeTaskWizard";
  let mode = "add_task";
  if (ctx.wizard.state.select_id || ctx.wizard.state.select_id === 0) {
    next_scene = "editOneTimeTaskWizard";
    mode = "edit_task";
  }
  ctx.wizard.state.interval = Interval[i];
  if (Interval[i] == Interval.ONCE) {
    ctx.scene.leave().then(function() {
      ctx.session[mode] = ctx.wizard.state[mode];
      ctx.session.next_scene = next_scene;
      showCalendar(ctx);
    });
  } else {
    ctx.wizard.next();
    return ctx.wizard.steps[ctx.wizard.cursor](ctx);
  }
});

const addTaskWizard = new WizardScene('addTaskWizard', function(ctx){
  ctx.replyWithPhoto(Emotion.CONCENTRATE, { caption: i18n.get(ctx.session.lang, 'add_task_name') });
  ctx.wizard.state.add_task = {};
  return ctx.wizard.next();
}, function(ctx) {
    if (ctx.message == null) {
      return;
    }
    let safe = checkString(ctx.message.text);
    if (!safe) {
      ctx.replyWithPhoto(Emotion.SPECIFY, { caption: i18n.get(ctx.session.lang, 'invalid_name', i18n.get(ctx.session.lang, ctx.session.gender)) });
      return;
    }
    ctx.telegram.sendMessage(ctx.chat.id, ctx.message.text + i18n.get(ctx.session.lang, 'add_task_name_conf'), { reply_markup: Markup.inlineKeyboard([
        [ Markup.callbackButton(i18n.get(ctx.session.lang, 'yes'), 'yes'), Markup.callbackButton(i18n.get(ctx.session.lang, 'no'), 'no') ],
      ])
    });
    ctx.wizard.state.add_task.name = ctx.message.text;
    return ctx.wizard.next();
  }, nameConfirmHandler, function(ctx) {
    ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'add_task_interval', i18n.get(ctx.session.lang, ctx.session.gender)), { reply_markup: Markup.inlineKeyboard([
        [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_0'), 'interval-once') ], 
        [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_1'), 'interval-daily') ],
        [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_2'), 'interval-weekly') ],
        [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_3'), 'interval-weekdays') ],
        [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_4'), 'interval-weekend') ],
      ])
    });
    return ctx.wizard.next();
  }, taskIntervalHandler, function(ctx) {
    ctx.reply(i18n.get(ctx.session.lang, 'add_task_limit', i18n.get(ctx.session.lang, ctx.session.gender)));
    return ctx.wizard.next();
  }, function(ctx) {
    let limit = checkNumber(ctx.message.text)
    if (!limit) {
      ctx.replyWithPhoto(Emotion.SPECIFY, { caption: i18n.get(ctx.session.lang, 'invalid_limit', i18n.get(ctx.session.lang, ctx.session.gender)) });
      return;
    }
    ctx.wizard.state.add_task.limit = +limit.toFixed(2);
    ctx.wizard.state.add_task.interval = ctx.wizard.state.interval;
    ctx.wizard.state.add_task.timestamp = Math.floor(new Date().getTime());
    insertTask(ctx);
    let reply = ctx.wizard.state.add_task.name + i18n.get(ctx.session.lang, 'comma');
    reply = reply + ' ' + i18n.get(ctx.session.lang, 'interval_' + ctx.wizard.state.add_task.interval) + i18n.get(ctx.session.lang, 'comma');
    reply = reply + ' ' + limit + ' ' + i18n.get(ctx.session.lang, 'hours');
    reply = reply + i18n.get(ctx.session.lang, 'done');
    ctx.replyWithPhoto(Emotion.GLAD, { caption: reply }).then(function(){
      ctx.scene.leave().then(function() {
        ctx.scene.enter('taskEditListRepeatWizard');
      });
    });
  });

const taskEditListHandler = new Composer();

taskEditListHandler.action('task_add', function(ctx) {
  ctx.scene.enter('addTaskWizard');
});

taskEditListHandler.action('task_edit', function(ctx) {
  ctx.scene.enter('editTaskWizard');
});

taskEditListHandler.action('task_enable', function(ctx) {
  ctx.scene.enter('enableTaskWizard');
});

taskEditListHandler.action('task_disable', function(ctx) {
  ctx.scene.enter('disableTaskWizard');
});

taskEditListHandler.action('task_remove', function(ctx) {
  let count = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id, remove: false }).size().value();
  if (count > 0) {
    ctx.scene.enter('removeTaskWizard');
  } else {
    ctx.wizard.back();
    return ctx.wizard.steps[ctx.wizard.cursor](ctx);
  }
});

taskEditListHandler.action('back', back);

const taskEditListWizard = new WizardScene('taskEditListWizard', function(ctx){
  let image_reply_markup = getTaskEditList(ctx);
  ctx.replyWithPhoto(image_reply_markup[0], { caption: image_reply_markup[1], parse_mode: 'HTML', reply_markup: Markup.inlineKeyboard(image_reply_markup[2]) });
  return ctx.wizard.next()
}, taskEditListHandler);

const taskEditListRepeatWizard = new WizardScene('taskEditListRepeatWizard', function(ctx){
  let image_reply_markup = getTaskEditList(ctx);
  ctx.replyWithHTML(image_reply_markup[1]).then(function() {
    ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'repeat'), { reply_markup: Markup.inlineKeyboard(image_reply_markup[2]) });
    ctx.wizard.next();
  });
}, taskEditListHandler);

const mainMenuHandler = new Composer();

mainMenuHandler.action('task_list', function(ctx) {
  ctx.scene.enter('taskListWizard');
});

mainMenuHandler.action('task_edit_list', function(ctx) {
  ctx.scene.enter('taskEditListWizard');
});

mainMenuHandler.action('set_timezone', function(ctx) {
  ctx.scene.enter('timeZoneWizard');
});

mainMenuHandler.action('lang', function(ctx) {
  ctx.scene.enter('langSelectWizard');
});

mainMenuHandler.action('help', function(ctx) {
  ctx.replyWithPhoto(Emotion.EXPLAIN, { caption: i18n.get(ctx.session.lang, 'help_text', i18n.get(ctx.session.lang, ctx.session.gender)), parse_mode: 'HTML' }).then(function() {
    ctx.scene.enter('mainMenuRepeatWizard');
  });
});

mainMenuHandler.action('gender', function(ctx) {
  ctx.scene.enter('genderSelectWizard');
});

const mainMenuWizard = new WizardScene('mainMenuWizard', function(ctx){
  if (!ctx.session.lang) {
    ctx.scene.enter('langSelectWizard');
    return;
  }
  if (!ctx.session.gender) {
    ctx.scene.enter('genderSelectWizard');
    return;
  }
  if (!ctx.session.timezone) {
    ctx.scene.enter('timeZoneWizard');
    return;
  }
  ctx.replyWithPhoto(Emotion.GREET, { caption: i18n.get(ctx.session.lang, 'greet', i18n.get(ctx.session.lang, ctx.session.gender)), reply_markup: getMainMenu(ctx) });
  return ctx.wizard.next();
}, mainMenuHandler);

const mainMenuRepeatWizard = new WizardScene('mainMenuRepeatWizard', function(ctx){
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'repeat'), { reply_markup: getMainMenu(ctx) });
  return ctx.wizard.next();
}, mainMenuHandler);

const taskChoiceHandler = new Composer();
taskChoiceHandler.action(/^[task]+(-[0-9]+)?$/, function(ctx) {
  ctx.wizard.state.select_id = +ctx.match[1].split('-')[1];
  ctx.wizard.next();
  return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});

const confirmHandler = new Composer();
confirmHandler.action('yes', function(ctx) {
  ctx.wizard.next();
  return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});
confirmHandler.action('no', function(ctx) {
  ctx.wizard.back();
  ctx.wizard.back();
  ctx.wizard.back();
  return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});


const removeTaskWizard = new WizardScene('removeTaskWizard', function(ctx){
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id, remove: false }).value();
  let markup = [];
  for (let i = 0; i < tasks.length; i++) {
    markup.push([ Markup.callbackButton(tasks[i].name + ' | ' + i18n.get(ctx.session.lang, 'interval_' + tasks[i].interval), 'task-' + tasks[i].id) ]);
  }
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'task_remove_choice'), { reply_markup: Markup.inlineKeyboard(markup) });
  return ctx.wizard.next();
}, taskChoiceHandler, function(ctx) {
  let task = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id, id: ctx.wizard.state.select_id }).value();
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'task_remove_conf') + ' ' + task[0].name, { reply_markup: Markup.inlineKeyboard([
      [ Markup.callbackButton(i18n.get(ctx.session.lang, 'yes'), 'yes'), Markup.callbackButton(i18n.get(ctx.session.lang, 'no'), 'no') ],
    ])
  });
  return ctx.wizard.next();
}, confirmHandler, function(ctx){
  ctx['sessionDB'].get('tasks').updateWhere({ user: ctx.chat.id, id: ctx.wizard.state.select_id }, { remove: true }).write();
  ctx.replyWithPhoto(Emotion.GLAD, { caption: i18n.get(ctx.session.lang, 'done') }).then(function(){
    ctx.scene.leave().then(function() {
      ctx.scene.enter("taskEditListRepeatWizard");
    });
  });
});

const editOneTimeTaskWizard = new WizardScene('editOneTimeTaskWizard', function(ctx){
  delete ctx.session.next_scene;
  ctx.reply(i18n.get(ctx.session.lang, 'add_task_time', i18n.get(ctx.session.lang, ctx.session.gender)));
  return ctx.wizard.next();
}, function(ctx) {
  let time = ctx.message.text;
  if(!/^(2[0-3]|[0-1]?[\d]):[0-5][\d]$/.test(time)) {
    ctx.replyWithPhoto(Emotion.SPECIFY, { caption: i18n.get(ctx.session.lang, 'invalid_limit', i18n.get(ctx.session.lang, ctx.session.gender)) });
    return;
  }
  let date = new Date(Date.parse(ctx.session.date));
  date.setHours(time.split(':')[0], time.split(':')[1]);
  ctx.wizard.state.edit_task = ctx.session.edit_task;
  let offset = getTimezoneOffset(date, ctx.session.timezone);
  ctx.wizard.state.edit_task.timestamp = Math.floor(date.getTime() + offset);
  ctx.wizard.state.edit_task.interval = Interval.ONCE;
  delete ctx.session.edit_task;
  delete ctx.session.date;
  ctx['sessionDB'].get('tasks').updateWhere({ user: ctx.chat.id, id: ctx.wizard.state.select_id }, { timestamp: ctx.wizard.state.edit_task.timestamp, interval: ctx.wizard.state.edit_task.interval }).write();
  let reply = ctx.wizard.state.edit_task.name + i18n.get(ctx.session.lang, 'comma');
  reply = reply + " " + date.toLocaleDateString(i18n.get(ctx.session.lang, 'locale'));
  reply = reply + i18n.get(ctx.session.lang, 'done');
  ctx.replyWithPhoto(Emotion.GLAD, { caption: reply }).then(function(){
    ctx.scene.leave().then(function() {
      ctx.scene.enter("taskEditListRepeatWizard");
    });
  });
});

const editTaskWizard = new WizardScene('editTaskWizard', function(ctx){
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id }).value();
  let markup = [];
  for (let i = 0; i < tasks.length; i++) {
    markup.push([ Markup.callbackButton(tasks[i].name + ' | ' + i18n.get(ctx.session.lang, 'interval_' + tasks[i].interval), 'task-' + tasks[i].id) ]);
  }
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'task_edit_choice'), { reply_markup: Markup.inlineKeyboard(markup) });
  return ctx.wizard.next();
}, taskChoiceHandler, function(ctx) {
  let task = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id, id: ctx.wizard.state.select_id }).value();
  ctx.wizard.state.edit_task = task[0];
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'add_task_interval', i18n.get(ctx.session.lang, ctx.session.gender)), { reply_markup: Markup.inlineKeyboard([
      [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_0'), 'interval-once') ], 
      [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_1'), 'interval-daily') ],
      [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_2'), 'interval-weekly') ],
      [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_3'), 'interval-weekdays') ],
      [ Markup.callbackButton(i18n.get(ctx.session.lang, 'interval_4'), 'interval-weekend') ],
    ])
  });
  return ctx.wizard.next();
}, taskIntervalHandler, function(ctx) {
  ctx.reply(i18n.get(ctx.session.lang, 'add_task_limit', i18n.get(ctx.session.lang, ctx.session.gender)));
  return ctx.wizard.next();
}, function(ctx) {
  let limit = checkNumber(ctx.message.text)
  if (!limit) {
    ctx.replyWithPhoto(Emotion.SPECIFY, { caption: i18n.get(ctx.session.lang, 'invalid_limit', i18n.get(ctx.session.lang, ctx.session.gender)) });
    return;
  }
  ctx.wizard.state.edit_task.limit = limit;
  ctx.wizard.state.edit_task.interval = ctx.wizard.state.interval;
  ctx['sessionDB'].get('tasks').updateWhere({ user: ctx.chat.id, id: ctx.wizard.state.select_id }, { limit: ctx.wizard.state.edit_task.limit, interval: ctx.wizard.state.edit_task.interval }).write();
  let reply = ctx.wizard.state.edit_task.name + i18n.get(ctx.session.lang, 'comma');
  reply = reply + ' ' + i18n.get(ctx.session.lang, 'interval_' + ctx.wizard.state.edit_task.interval) + i18n.get(ctx.session.lang, 'comma');
  reply = reply + ' ' + limit + ' ' + i18n.get(ctx.session.lang, 'hours');
  reply = reply + i18n.get(ctx.session.lang, 'done');
  ctx.replyWithPhoto(Emotion.GLAD, { caption: reply }).then(function(){
    ctx.scene.leave().then(function() {
      ctx.scene.enter("taskEditListRepeatWizard");
    });
  });
});

const enableTaskWizard = new WizardScene('enableTaskWizard', function(ctx){
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id, enabled: false }).value();
  if (tasks.length < 1) {
    ctx.replyWithPhoto(Emotion.EXCUSE, { caption: i18n.get(ctx.session.lang, 'no_disabled_tasks', i18n.get(ctx.session.lang, ctx.session.gender)) }).then(function(){
      ctx.scene.leave().then(function() {
        ctx.scene.enter('taskEditListRepeatWizard');
      });
    });
    return;
  }
  let markup = [];
  for (let i = 0; i < tasks.length; i++) {
    markup.push([ Markup.callbackButton(tasks[i].name + ' | ' + i18n.get(ctx.session.lang, 'interval_' + tasks[i].interval), 'task-' + tasks[i].id) ]);
  }
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'task_enable_choice'), { reply_markup: Markup.inlineKeyboard(markup) });
  return ctx.wizard.next();
}, taskChoiceHandler, function(ctx) {
  ctx['sessionDB'].get('tasks').updateWhere({ user: ctx.chat.id, id: ctx.wizard.state.select_id }, { enabled: true }).write();
  ctx.replyWithPhoto(Emotion.GLAD, { caption: i18n.get(ctx.session.lang, 'done') }).then(function(){
    ctx.scene.leave().then(function() {
      ctx.scene.enter('taskEditListRepeatWizard');
    });
  });
});

const disableTaskWizard = new WizardScene('disableTaskWizard', function(ctx){
  let tasks = ctx['sessionDB'].get('tasks').filter({ user: ctx.chat.id, enabled: true }).value();
  if (tasks.length < 1) {
    ctx.replyWithPhoto(Emotion.EXCUSE, { caption: i18n.get(ctx.session.lang, 'no_enabled_tasks', i18n.get(ctx.session.lang, ctx.session.gender)) }).then(function(){
      ctx.scene.leave().then(function() {
        ctx.scene.enter('taskEditListRepeatWizard');
      });
    });
    return;
  }
  let markup = [];
  for (let i = 0; i < tasks.length; i++) {
    markup.push([ Markup.callbackButton(tasks[i].name + ' | ' + i18n.get(ctx.session.lang, 'interval_' + tasks[i].interval), 'task-' + tasks[i].id) ]);
  }
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'task_disable_choice'), { reply_markup: Markup.inlineKeyboard(markup) });
  return ctx.wizard.next();
}, taskChoiceHandler, function(ctx) {
  ctx['sessionDB'].get('tasks').updateWhere({ user: ctx.chat.id, id: ctx.wizard.state.select_id }, { enabled: false }).write();
  ctx.replyWithPhoto(Emotion.GLAD, { caption: i18n.get(ctx.session.lang, 'done') }).then(function(){
    ctx.scene.leave().then(function() {
      ctx.scene.enter('taskEditListRepeatWizard');
    });
  });
});

const timeZoneHandler = new Composer();
timeZoneHandler.action(/^[timezone]+(-[a-zA-Z_/]+)?$/, function(ctx) {
  ctx.session.timezone = ctx.match[1].split('-')[1];
  ctx.scene.leave().then(function() {
    ctx.scene.enter("mainMenuRepeatWizard");
  });
});
timeZoneHandler.action("back", function(ctx) {
  ctx.scene.leave().then(function() {
    ctx.scene.enter("mainMenuRepeatWizard");
  });
});
timeZoneHandler.action("next", function(ctx) {
  ctx.session.tz_offset = ctx.session.tz_offset + 1;
  if (ctx.session.tz_offset > Math.floor(TimeZones.length/10)) {
    ctx.session.tz_offset = 0;
  }
  ctx.wizard.back();
  return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});
timeZoneHandler.action("previous", function(ctx) {
  ctx.session.tz_offset = ctx.session.tz_offset + 1;
  if (ctx.session.tz_offset < 0) {
    ctx.session.tz_offset = Math.floor(TimeZones.length/10);
  }
  ctx.wizard.back();
  return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});

const timeZoneWizard = new WizardScene('timeZoneWizard', function(ctx){
  if (!ctx.session.tz_offset) {
    ctx.session.tz_offset = 0;
  }
  let markup = [];
  let offset = ctx.session.tz_offset;
  for (let i = 0; i < 10; i++) {
    markup.push([ Markup.callbackButton(TimeZones[i + (10 * offset)], "timezone-" + TimeZones[i + (10 * offset)]) ]);
  }
  markup.push([ Markup.callbackButton("◀️", "previous"), Markup.callbackButton(i18n.get(ctx.session.lang, 'back'), "back"), Markup.callbackButton("▶️", "next") ]);
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(ctx.session.lang, 'timezone_choice', i18n.get(ctx.session.lang, ctx.session.gender)), { reply_markup: Markup.inlineKeyboard(markup) });
  return ctx.wizard.next();
}, timeZoneHandler);

const langSelectHandler = new Composer();
langSelectHandler.action('en', function(ctx) {
  ctx.session.lang = 'en';
  ctx.scene.leave().then(function() {
    ctx.scene.enter('mainMenuWizard');
  });
});
langSelectHandler.action('ru', function(ctx) {
  ctx.session.lang = 'ru';
  ctx.scene.leave().then(function() {
    ctx.scene.enter('mainMenuWizard');
  });
});
const langSelectWizard = new WizardScene('langSelectWizard', function(ctx){
  let markup = [];
  markup.push([
    Markup.callbackButton('🇬🇧 English', 'en'),
    Markup.callbackButton('🇷🇺 Русский', 'ru'),
  ]);
  let lang = ctx.session.lang || 'en';
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(lang, 'select_lang'), { reply_markup: Markup.inlineKeyboard(markup) });
  return ctx.wizard.next();
}, langSelectHandler);

const genderSelectHandler = new Composer();
genderSelectHandler.action('master', function(ctx) {
  ctx.session.gender = 'master';
  ctx.scene.leave().then(function() {
    ctx.scene.enter('mainMenuWizard');
  });
});
genderSelectHandler.action('mistress', function(ctx) {
  ctx.session.gender = 'mistress';
  ctx.scene.leave().then(function() {
    ctx.scene.enter('mainMenuWizard');
  });
});
const genderSelectWizard = new WizardScene('genderSelectWizard', function(ctx){
  let markup = [];
  markup.push([
    Markup.callbackButton(GenderIcons.master + ' ' + i18n.get(ctx.session.lang, 'master'), 'master'),
    Markup.callbackButton(GenderIcons.mistress + ' ' + i18n.get(ctx.session.lang, 'mistress'), 'mistress'),
  ]);
  let lang = ctx.session.lang || 'en';
  ctx.telegram.sendMessage(ctx.chat.id, i18n.get(lang, 'select_gender'), { reply_markup: Markup.inlineKeyboard(markup) });
  return ctx.wizard.next();
}, genderSelectHandler);

function loadConfig(callback) {
  if(fs.existsSync('config.json')){
    let data = fs.readFileSync('config.json');
    global._config = JSON.parse(data);
    if(global._config) {
      if (global._config.token) {
        if(callback) {
          callback();
        }
      } else {
        console.log('[ERROR] configuration file must contain bot API token, specify it and start this app again.');
      }
    }
  }
  if (!global._config) {
    console.log('[WARNING] configuration file not found, generating a new one...');
    global._config = { token: "" };
    fs.writeFileSync('config.json', JSON.stringify(global._config));
    console.log('[WARNING] done.');
    console.log('[ERROR] configuration file must contain bot API token, specify it and start this app again.');
  }
}

function botStart(ctx) {
  ctx.scene.enter('mainMenuWizard');
}

function handleMessage(ctx) {
  let scene = ctx.session.__scenes.current;
  if (!scene||scene=="mainMenuWizard"||scene=="mainMenuRepeatWizard") {
    ctx.scene.enter('mainMenuWizard');
  }
}

function initBot() {
  _bot = new Telegraf(global._config.token);
  _session = new LocalSession({
    database: 'db.json',
    state: { tasks: [] }
  });
  _bot.use(_session);
  _bot.use(new Stage([ mainMenuWizard, mainMenuRepeatWizard, taskEditListWizard, taskEditListRepeatWizard, addTaskWizard, addOneTimeTaskWizard, removeTaskWizard, editTaskWizard, editOneTimeTaskWizard, taskListWizard, taskListRepeatWizard, taskTrackWizard, timeZoneWizard, langSelectWizard, genderSelectWizard, enableTaskWizard, disableTaskWizard ]));
  _bot.start(botStart);
  _bot.on("message", handleMessage);
  _bot.startPolling();
  console.log('[INFO] tg-maid bot started.');
  setInterval(checkLoop, 60000);
}

function init() {
  loadConfig(initBot);
}

init();

//TODO: add picture captioning
